/* sp-ssh-connection-private.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SP_SSH_CONNECTION_PRIVATE_H
#define SP_SSH_CONNECTION_PRIVATE_H

#include "sp-ssh-connection.h"
#include "sp-ssh-credentials.h"

G_BEGIN_DECLS

SpSshConnection *_sp_ssh_connection_new                 (GSocketConnection    *connection) G_GNUC_INTERNAL;
void             _sp_ssh_connection_authenticate_async  (SpSshConnection      *self,
                                                         SpSshCredentials     *credentials,
                                                         GCancellable         *cancellable,
                                                         GAsyncReadyCallback   callback,
                                                         gpointer              user_data)  G_GNUC_INTERNAL;
gboolean         _sp_ssh_connection_authenticate_finish (SpSshConnection      *self,
                                                         GAsyncResult         *result,
                                                         GError              **error)      G_GNUC_INTERNAL;
void             _sp_ssh_connection_handshake_async     (SpSshConnection      *self,
                                                         GCancellable         *cancellable,
                                                         GAsyncReadyCallback   callback,
                                                         gpointer              user_data)  G_GNUC_INTERNAL;
gboolean         _sp_ssh_connection_handshake_finish    (SpSshConnection      *self,
                                                         GAsyncResult         *result,
                                                         GError              **error)      G_GNUC_INTERNAL;

G_END_DECLS

#endif /* SP_SSH_CONNECTION_PRIVATE_H */
