/* sp-ssh-agent-credentials.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gio/gio.h>
#include <libssh2.h>

#include "sp-ssh-agent-credentials.h"
#include "sp-ssh-error.h"

struct _SpSshAgentCredentials
{
  GObject  parent_instance;
  gchar   *username;
};

enum {
  PROP_0,
  PROP_USERNAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

const gchar *
sp_ssh_agent_credentials_get_username (SpSshAgentCredentials *self)
{
  g_return_val_if_fail (SP_IS_SSH_AGENT_CREDENTIALS (self), NULL);

  if (self->username != NULL)
    return self->username;

  return g_get_user_name ();
}

static gboolean
userauthlist_contains (const gchar *userauthlist,
                       const gchar *scheme)
{
  g_auto(GStrv) schemes = NULL;
  guint i;

  if (userauthlist == NULL)
    return FALSE;

  schemes = g_strsplit (userauthlist, ",", 0);

  for (i = 0; schemes[i]; i++)
    {
      if (g_str_equal (schemes[i], scheme))
        return TRUE;
    }

  return FALSE;
}

static gboolean
sp_ssh_agent_credentials_authenticate (SpSshCredentials  *credentials,
                                       gpointer           native,
                                       GError           **error)
{
  SpSshAgentCredentials *self = (SpSshAgentCredentials *)credentials;
  struct libssh2_agent_publickey *identity = NULL;
  struct libssh2_agent_publickey *previous = NULL;
  LIBSSH2_SESSION *session = native;
  LIBSSH2_AGENT *agent = NULL;
  const gchar *username;
  const gchar *userauthlist;
  gboolean ret = FALSE;
  gint rc;

  g_assert (SP_IS_SSH_AGENT_CREDENTIALS (self));
  g_assert (native != NULL);

  username = sp_ssh_agent_credentials_get_username (self);
  userauthlist = libssh2_userauth_list (session, username, strlen (username));

  if (userauthlist == NULL)
    {
      /*
       * It's possible that the NONE authorization succeeded (which is what is
       * used to get the list of available authentication schemes). Check to
       * see if it worked before assuming the worst.
       */
      if (libssh2_userauth_authenticated (session))
        return TRUE;
    }

  if (!userauthlist_contains (userauthlist, "publickey"))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_FAILED,
                   "publickey auth is not supported by SSH peer");
      return FALSE;
    }

  if (NULL == (agent = libssh2_agent_init (session)))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_FAILED,
                   "Failed to initialize SSH agent support");
      return FALSE;
    }

  if (0 != (rc = libssh2_agent_connect (agent)))
    {
      g_set_error (error,
                   SP_SSH_ERROR,
                   rc,
                   "Failed to connect to SSH agent");
      goto cleanup;
    }

  if (0 != (rc = libssh2_agent_list_identities (agent)))
    {
      g_set_error (error,
                   SP_SSH_ERROR,
                   rc,
                   "Failed to list available identifies in SSH agent");
      goto disconnect;
    }

  for (;;)
    {
      rc = libssh2_agent_get_identity (agent, &identity, previous);

      if (rc == 1)
        {
          g_set_error (error,
                       SP_SSH_ERROR,
                       rc,
                       "No more identities to try");
          goto disconnect;
        }
      else if (rc < 0)
        {
          g_set_error (error,
                       SP_SSH_ERROR,
                       rc,
                       "Failure obtaining identity from SSH agent");
          goto disconnect;
        }

      if (0 == libssh2_agent_userauth (agent, username, identity))
        break;

      previous = identity;
    }

  ret = TRUE;

disconnect:
  libssh2_agent_disconnect (agent);

cleanup:
  if (agent != NULL)
    libssh2_agent_free (agent);

  return ret;
}

static void
credentials_iface_init (SpSshCredentialsInterface *iface)
{
  iface->authenticate = sp_ssh_agent_credentials_authenticate;
}

G_DEFINE_TYPE_EXTENDED (SpSshAgentCredentials, sp_ssh_agent_credentials, G_TYPE_OBJECT, 0,
                        G_IMPLEMENT_INTERFACE (SP_TYPE_SSH_CREDENTIALS, credentials_iface_init))

static void
sp_ssh_agent_credentials_finalize (GObject *object)
{
  SpSshAgentCredentials *self = (SpSshAgentCredentials *)object;

  g_clear_pointer (&self->username, g_free);

  G_OBJECT_CLASS (sp_ssh_agent_credentials_parent_class)->finalize (object);
}

static void
sp_ssh_agent_credentials_get_property (GObject    *object,
                                       guint       prop_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
  SpSshAgentCredentials *self = SP_SSH_AGENT_CREDENTIALS (object);

  switch (prop_id)
    {
    case PROP_USERNAME:
      g_value_set_string (value, sp_ssh_agent_credentials_get_username (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sp_ssh_agent_credentials_set_property (GObject      *object,
                                       guint         prop_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
  SpSshAgentCredentials *self = SP_SSH_AGENT_CREDENTIALS (object);

  switch (prop_id)
    {
    case PROP_USERNAME:
      self->username = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sp_ssh_agent_credentials_class_init (SpSshAgentCredentialsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = sp_ssh_agent_credentials_finalize;
  object_class->get_property = sp_ssh_agent_credentials_get_property;
  object_class->set_property = sp_ssh_agent_credentials_set_property;

  properties [PROP_USERNAME] =
    g_param_spec_string ("username",
                         "Username",
                         "Username",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
sp_ssh_agent_credentials_init (SpSshAgentCredentials *self)
{
}

SpSshCredentials *
sp_ssh_agent_credentials_new (const gchar *username)
{
  return g_object_new (SP_TYPE_SSH_AGENT_CREDENTIALS,
                       "username", username,
                       NULL);
}
