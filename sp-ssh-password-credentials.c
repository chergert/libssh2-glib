/* sp-ssh-password-credentials.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gio/gio.h>
#include <libssh2.h>

#include "sp-ssh-password-credentials.h"

struct _SpSshPasswordCredentials
{
  GObject parent_instance;
  gchar *password;
  gchar *username;
};

enum {
  PROP_0,
  PROP_PASSWORD,
  PROP_USERNAME,
  N_PROPS
};

static void     credentials_iface_init                   (SpSshCredentialsInterface  *iface);
static void     sp_ssh_password_credentials_finalize     (GObject                    *object);
static gboolean sp_ssh_password_credentials_authenticate (SpSshCredentials           *self,
                                                          gpointer                    native,
                                                          GError                    **error);

G_DEFINE_TYPE_EXTENDED (SpSshPasswordCredentials, sp_ssh_password_credentials, G_TYPE_OBJECT, 0,
                        G_IMPLEMENT_INTERFACE (SP_TYPE_SSH_CREDENTIALS, credentials_iface_init))

static GParamSpec *properties [N_PROPS];

static void
credentials_iface_init (SpSshCredentialsInterface *iface)
{
  iface->authenticate = sp_ssh_password_credentials_authenticate;
}

static gboolean
sp_ssh_password_credentials_authenticate (SpSshCredentials  *credentials,
                                          gpointer           native,
                                          GError           **error)
{
  SpSshPasswordCredentials *self = (SpSshPasswordCredentials *)credentials;
  LIBSSH2_SESSION *session = native;

  g_assert (SP_IS_SSH_PASSWORD_CREDENTIALS (self));
  g_assert (session != NULL);

  for (;;)
    {
      gint rc;

      rc = libssh2_userauth_password (session, self->username, self->password);

      if (rc == LIBSSH2_ERROR_EAGAIN)
        continue;

      if (rc != 0)
        {
          g_set_error (error,
                       G_IO_ERROR,
                       G_IO_ERROR_PERMISSION_DENIED,
                       "Password authentication failed");
          return FALSE;
        }

      return TRUE;
    }
}

static void
sp_ssh_password_credentials_finalize (GObject *object)
{
  SpSshPasswordCredentials *self = (SpSshPasswordCredentials *)object;

  g_clear_pointer (&self->password, g_free);
  g_clear_pointer (&self->username, g_free);

  G_OBJECT_CLASS (sp_ssh_password_credentials_parent_class)->finalize (object);
}

static void
sp_ssh_password_credentials_get_property (GObject    *object,
                                          guint       prop_id,
                                          GValue     *value,
                                          GParamSpec *pspec)
{
  SpSshPasswordCredentials *self = (SpSshPasswordCredentials *)object;

  switch (prop_id)
    {
    case PROP_PASSWORD:
      g_value_set_string (value, sp_ssh_password_credentials_get_password (self));
      break;

    case PROP_USERNAME:
      g_value_set_string (value, sp_ssh_password_credentials_get_username (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sp_ssh_password_credentials_set_property (GObject      *object,
                                          guint         prop_id,
                                          const GValue *value,
                                          GParamSpec   *pspec)
{
  SpSshPasswordCredentials *self = (SpSshPasswordCredentials *)object;

  switch (prop_id)
    {
    case PROP_PASSWORD:
      self->password = g_value_dup_string (value);
      break;

    case PROP_USERNAME:
      self->username = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sp_ssh_password_credentials_class_init (SpSshPasswordCredentialsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = sp_ssh_password_credentials_finalize;
  object_class->get_property = sp_ssh_password_credentials_get_property;
  object_class->set_property = sp_ssh_password_credentials_set_property;

  properties [PROP_PASSWORD] =
    g_param_spec_string ("password",
                         "Password",
                         "Password",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  properties [PROP_USERNAME] =
    g_param_spec_string ("username",
                         "Username",
                         "Username",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
sp_ssh_password_credentials_init (SpSshPasswordCredentials *self)
{
}

const gchar *
sp_ssh_password_credentials_get_password (SpSshPasswordCredentials *self)
{
  g_return_val_if_fail (SP_IS_SSH_PASSWORD_CREDENTIALS (self), NULL);

  return self->password;
}

const gchar *
sp_ssh_password_credentials_get_username (SpSshPasswordCredentials *self)
{
  g_return_val_if_fail (SP_IS_SSH_PASSWORD_CREDENTIALS (self), NULL);

  return self->username;
}

SpSshCredentials *
sp_ssh_password_credentials_new (const gchar *username,
                                 const gchar *password)
{
  g_return_val_if_fail (username != NULL, NULL);

  if (password == NULL)
    password = "";

  return g_object_new (SP_TYPE_SSH_PASSWORD_CREDENTIALS,
                       "username", username,
                       "password", password,
                       NULL);
}
