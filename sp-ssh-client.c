/* sp-ssh-client.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sp-ssh-agent-credentials.h"
#include "sp-ssh-client.h"
#include "sp-ssh-connection-private.h"
#include "sp-ssh-credentials.h"
#include "sp-ssh-password-credentials.h"

/**
 * SECTION:sp-ssh-client
 * @title: SpSshClient
 * @short_description: A SSH client for Gio
 *
 * This class provides a wrapper around libssh2 using the socket primatives
 * provided by Gio-2.0.
 *
 * This class is not thread-safe, but is thread-aware. You can create and
 * use #SpSshClient from any thread, but you must use it from only a single
 * thread at a time.
 */

G_DEFINE_TYPE (SpSshClient, sp_ssh_client, G_TYPE_SOCKET_CLIENT)

static void              sp_ssh_client_event                      (GSocketClient        *client,
                                                                   GSocketClientEvent    event,
                                                                   GSocketConnectable   *connectable,
                                                                   GIOStream            *connection);
static void              sp_ssh_client_finalize                   (GObject              *object);
static void              sp_ssh_client_connect_cb                 (GObject              *object,
                                                                   GAsyncResult         *result,
                                                                   gpointer              user_data);
static void              sp_ssh_client_handshake_cb               (GObject              *object,
                                                                   GAsyncResult         *result,
                                                                   gpointer              user_data);
static void              sp_ssh_client_real_get_credentials_async (SpSshClient          *self,
                                                                   SpSshConnection      *connection,
                                                                   GCancellable         *cancellable,
                                                                   GAsyncReadyCallback   callback,
                                                                   gpointer              user_data);
static SpSshCredentials *sp_ssh_client_real_get_credentials_finish (SpSshClient          *self,
                                                                   GAsyncResult         *result,
                                                                   GError              **error);
static void              sp_ssh_client_get_credentials_async      (SpSshClient          *self,
                                                                   SpSshConnection      *connection,
                                                                   GCancellable         *cancellable,
                                                                   GAsyncReadyCallback   callback,
                                                                   gpointer              user_data);
static SpSshCredentials *sp_ssh_client_get_credentials_finish     (SpSshClient          *self,
                                                                   GAsyncResult         *result,
                                                                   GError              **error);
static void              sp_ssh_client_next_creds_cb              (GObject              *object,
                                                                   GAsyncResult         *result,
                                                                   gpointer              user_data);

static void
sp_ssh_client_event (GSocketClient      *client,
                     GSocketClientEvent  event,
                     GSocketConnectable *connectable,
                     GIOStream          *connection)
{
  g_assert (G_IS_SOCKET_CLIENT (client));
  g_assert (G_IS_SOCKET_CONNECTABLE (connectable));
  g_assert (!connection || G_IS_IO_STREAM (connection));

  switch (event)
    {
    case G_SOCKET_CLIENT_CONNECTED:
      {
        GSocket *sock;
        gint fd;

        g_assert (G_IS_SOCKET_CONNECTION (connection));

        sock = g_socket_connection_get_socket (G_SOCKET_CONNECTION (connection));
        g_assert (G_IS_SOCKET (sock));

        fd = g_socket_get_fd (sock);
        g_assert (fd != -1);


      }
      break;

    case G_SOCKET_CLIENT_RESOLVING:
    case G_SOCKET_CLIENT_RESOLVED:
    case G_SOCKET_CLIENT_CONNECTING:
    case G_SOCKET_CLIENT_PROXY_NEGOTIATING:
    case G_SOCKET_CLIENT_PROXY_NEGOTIATED:
    case G_SOCKET_CLIENT_TLS_HANDSHAKING:
    case G_SOCKET_CLIENT_TLS_HANDSHAKED:
    case G_SOCKET_CLIENT_COMPLETE:
    default:
      break;
    }

  if (G_SOCKET_CLIENT_CLASS (sp_ssh_client_parent_class)->event)
    G_SOCKET_CLIENT_CLASS (sp_ssh_client_parent_class)->event (client, event, connectable, connection);
}

static void
sp_ssh_client_finalize (GObject *object)
{
  G_OBJECT_CLASS (sp_ssh_client_parent_class)->finalize (object);
}

static void
sp_ssh_client_class_init (SpSshClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GSocketClientClass *socket_client_class = G_SOCKET_CLIENT_CLASS (klass);

  object_class->finalize = sp_ssh_client_finalize;

  socket_client_class->event = sp_ssh_client_event;

  klass->get_credentials_async = sp_ssh_client_real_get_credentials_async;
  klass->get_credentials_finish = sp_ssh_client_real_get_credentials_finish;
}

static void
sp_ssh_client_init (SpSshClient *self)
{
}

SpSshClient *
sp_ssh_client_new (void)
{
  return g_object_new (SP_TYPE_SSH_CLIENT, NULL);
}

static void
sp_ssh_client_authenticate_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  SpSshConnection *sshconn = (SpSshConnection *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  SpSshClient *self;
  GCancellable *cancellable;

  g_assert (SP_IS_SSH_CONNECTION (sshconn));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  self = g_task_get_source_object (task);
  g_assert (SP_IS_SSH_CLIENT (self));

  cancellable = g_task_get_cancellable (task);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (!_sp_ssh_connection_authenticate_finish (sshconn, result, &error))
    {
      g_printerr ("%s\n", error->message);

      sp_ssh_client_get_credentials_async (self,
                                           sshconn,
                                           cancellable,
                                           sp_ssh_client_next_creds_cb,
                                           g_steal_pointer (&task));
      return;
    }

  g_task_return_pointer (task, g_object_ref (sshconn), g_object_unref);
}

static void
sp_ssh_client_next_creds_cb (GObject      *object,
                             GAsyncResult *result,
                             gpointer      user_data)
{
  SpSshClient *self = (SpSshClient *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(SpSshCredentials) credentials = NULL;
  g_autoptr(GError) error = NULL;
  SpSshConnection *sshconn;
  GCancellable *cancellable;

  g_assert (SP_IS_SSH_CLIENT (self));
  g_assert (G_IS_TASK (task));

  sshconn = g_task_get_task_data (task);
  g_assert (SP_IS_SSH_CONNECTION (sshconn));

  cancellable = g_task_get_cancellable (task);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  credentials = sp_ssh_client_get_credentials_finish (self, result, &error);
  if (credentials == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  _sp_ssh_connection_authenticate_async (sshconn,
                                         credentials,
                                         cancellable,
                                         sp_ssh_client_authenticate_cb,
                                         g_steal_pointer (&task));
}

static void
sp_ssh_client_handshake_cb (GObject      *object,
                            GAsyncResult *result,
                            gpointer      user_data)
{
  SpSshConnection *sshconn = (SpSshConnection *)object;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  GCancellable *cancellable;
  SpSshClient *self;

  g_assert (SP_IS_SSH_CONNECTION (sshconn));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (G_IS_TASK (task));

  if (!_sp_ssh_connection_handshake_finish (sshconn, result, &error))
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  cancellable = g_task_get_cancellable (task);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  self = g_task_get_source_object (task);
  g_assert (SP_IS_SSH_CLIENT (self));

  g_task_set_task_data (task, g_object_ref (sshconn), g_object_unref);

  sp_ssh_client_get_credentials_async (self,
                                       sshconn,
                                       cancellable,
                                       sp_ssh_client_next_creds_cb,
                                       g_steal_pointer (&task));
}

static void
sp_ssh_client_connect_cb (GObject      *object,
                          GAsyncResult *result,
                          gpointer      user_data)
{
  GSocketClient *client = (GSocketClient *)object;
  g_autoptr(GSocketConnection) connection = NULL;
  g_autoptr(GTask) task = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(SpSshConnection) sshconn = NULL;
  GCancellable *cancellable;

  g_assert (SP_IS_SSH_CLIENT (client));

  connection = g_socket_client_connect_finish (client, result, &error);

  if (connection == NULL)
    {
      g_task_return_error (task, g_steal_pointer (&error));
      return;
    }

  cancellable = g_task_get_cancellable (task);

  sshconn = _sp_ssh_connection_new (connection);

  _sp_ssh_connection_handshake_async (sshconn,
                                      cancellable,
                                      sp_ssh_client_handshake_cb,
                                      g_steal_pointer (&task));
}

void
sp_ssh_client_connect_async (SpSshClient         *self,
                             GSocketConnectable  *connectable,
                             GCancellable        *cancellable,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (SP_IS_SSH_CLIENT (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, sp_ssh_client_connect_async);

  g_socket_client_connect_async (G_SOCKET_CLIENT (self),
                                 connectable,
                                 cancellable,
                                 sp_ssh_client_connect_cb,
                                 g_steal_pointer (&task));
}

/**
 * sp_ssh_client_connect_finish:
 *
 * Completes an asynchronous request to connect to the destination. See
 * sp_ssh_client_connect_async() for more information.
 *
 * Returns: (transfer full): An #SpSshConnection if successful; otherwise
 *   %NULL and @error is set.
 */
SpSshConnection *
sp_ssh_client_connect_finish (SpSshClient   *self,
                              GAsyncResult  *result,
                              GError       **error)
{
  g_return_val_if_fail (SP_IS_SSH_CLIENT (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}

static inline gboolean
has_tried_auth (SpSshConnection *connection,
                GType            credentials_type)
{
  g_autofree gchar *key = NULL;

  g_assert (SP_IS_SSH_CONNECTION (connection));
  g_assert (g_type_is_a (credentials_type, SP_TYPE_SSH_CREDENTIALS));

  key = g_strdup_printf ("SP_SSH_CREDENTIALS:%s", g_type_name (credentials_type));
  return !!g_object_get_data (G_OBJECT (connection), key);
}

static inline void
mark_auth_tried (SpSshConnection *connection,
                 GType            credentials_type)
{
  g_autofree gchar *key = NULL;

  g_assert (SP_IS_SSH_CONNECTION (connection));
  g_assert (g_type_is_a (credentials_type, SP_TYPE_SSH_CREDENTIALS));

  key = g_strdup_printf ("SP_SSH_CREDENTIALS:%s", g_type_name (credentials_type));
  g_object_set_data (G_OBJECT (connection), key, GINT_TO_POINTER (1));
}

static void
sp_ssh_client_real_get_credentials_async (SpSshClient         *self,
                                          SpSshConnection     *connection,
                                          GCancellable        *cancellable,
                                          GAsyncReadyCallback  callback,
                                          gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (SP_IS_SSH_CLIENT (self));
  g_return_if_fail (SP_IS_SSH_CONNECTION (connection));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, sp_ssh_client_get_credentials_async);

  if (!has_tried_auth (connection, SP_TYPE_SSH_AGENT_CREDENTIALS))
    {
      mark_auth_tried (connection, SP_TYPE_SSH_AGENT_CREDENTIALS);
      g_task_return_pointer (task, sp_ssh_agent_credentials_new (NULL), g_object_unref);
      return;
    }

  g_task_return_new_error (task,
                           G_IO_ERROR,
                           G_IO_ERROR_PERMISSION_DENIED,
                           "No more authentication protocols to try");
}

static SpSshCredentials *
sp_ssh_client_real_get_credentials_finish (SpSshClient   *self,
                                           GAsyncResult  *result,
                                           GError       **error)
{
  g_return_val_if_fail (SP_IS_SSH_CLIENT (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  return g_task_propagate_pointer (G_TASK (result), error);
}

static void
sp_ssh_client_get_credentials_async (SpSshClient         *self,
                                     SpSshConnection     *connection,
                                     GCancellable        *cancellable,
                                     GAsyncReadyCallback  callback,
                                     gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;
  g_autoptr(SpSshCredentials) creds = NULL;

  g_return_if_fail (SP_IS_SSH_CLIENT (self));
  g_return_if_fail (SP_IS_SSH_CONNECTION (connection));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  SP_SSH_CLIENT_GET_CLASS (self)->get_credentials_async (self, connection, cancellable, callback, user_data);
}

static SpSshCredentials *
sp_ssh_client_get_credentials_finish (SpSshClient   *self,
                                      GAsyncResult  *result,
                                      GError       **error)
{
  g_return_val_if_fail (SP_IS_SSH_CLIENT (self), NULL);
  g_return_val_if_fail (G_IS_TASK (result), NULL);

  return SP_SSH_CLIENT_GET_CLASS (self)->get_credentials_finish (self, result, error);
}
