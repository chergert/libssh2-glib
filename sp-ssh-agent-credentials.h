/* sp-ssh-agent-credentials.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SP_SSH_AGENT_CREDENTIALS_H
#define SP_SSH_AGENT_CREDENTIALS_H

#include "sp-ssh-credentials.h"

G_BEGIN_DECLS

#define SP_TYPE_SSH_AGENT_CREDENTIALS (sp_ssh_agent_credentials_get_type())

G_DECLARE_FINAL_TYPE (SpSshAgentCredentials, sp_ssh_agent_credentials, SP, SSH_AGENT_CREDENTIALS, GObject)

SpSshCredentials *sp_ssh_agent_credentials_new          (const gchar           *username);
const gchar      *sp_ssh_agent_credentials_get_username (SpSshAgentCredentials *self);

G_END_DECLS

#endif /* SP_SSH_AGENT_CREDENTIALS_H */
