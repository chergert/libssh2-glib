all: test-client

FILES = \
	sp-ssh-agent-credentials.c \
	sp-ssh-agent-credentials.h \
	sp-ssh-client.c \
	sp-ssh-client.h \
	sp-ssh-connection.c \
	sp-ssh-connection.h \
	sp-ssh-connection-private.h \
	sp-ssh-credentials.c \
	sp-ssh-credentials.h \
	sp-ssh-error.c \
	sp-ssh-error.h \
	sp-ssh-password-credentials.c \
	sp-ssh-password-credentials.h \
	sp-ssh-types.h \
	sp-ssh.h \
	main.c

WARNINGS = -Wall
PKGS = gio-2.0 libssh2

test-client: $(FILES) Makefile
	$(CC) -o $@ -std=gnu11 $(WARNINGS) $(FILES) $(shell pkg-config --cflags --libs $(PKGS))

clean:
	rm -f test-client
