/* sp-ssh-client.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SP_SSH_CLIENT_H
#define SP_SSH_CLIENT_H

#include <gio/gio.h>

#include "sp-ssh-types.h"

G_BEGIN_DECLS

#define SP_TYPE_SSH_CLIENT (sp_ssh_client_get_type())

G_DECLARE_DERIVABLE_TYPE (SpSshClient, sp_ssh_client, SP, SSH_CLIENT, GSocketClient)

struct _SpSshClientClass
{
  GSocketClientClass parent_class;

  void              (*get_credentials_async)  (SpSshClient          *self,
                                               SpSshConnection      *connection,
                                               GCancellable         *cancellable,
                                               GAsyncReadyCallback   callback,
                                               gpointer              user_data);
  SpSshCredentials *(*get_credentials_finish) (SpSshClient          *self,
                                               GAsyncResult         *result,
                                               GError              **error);
  gpointer _reserved1;
  gpointer _reserved2;
  gpointer _reserved3;
  gpointer _reserved4;
  gpointer _reserved5;
  gpointer _reserved6;
  gpointer _reserved7;
  gpointer _reserved8;
};

SpSshClient     *sp_ssh_client_new            (void);
void             sp_ssh_client_connect_async  (SpSshClient          *self,
                                               GSocketConnectable   *connectable,
                                               GCancellable         *cancellable,
                                               GAsyncReadyCallback   callback,
                                               gpointer              user_data);
SpSshConnection *sp_ssh_client_connect_finish (SpSshClient          *self,
                                               GAsyncResult         *result,
                                               GError              **error);

G_END_DECLS

#endif /* SP_SSH_CLIENT_H */
