#include <libssh2.h>

#include "sp-ssh.h"

static GMainLoop *main_loop;

static void
on_close_cb (GObject      *object,
             GAsyncResult *result,
             gpointer      user_data)
{
  SpSshConnection *connection = (SpSshConnection *)object;
  g_autoptr(GError) error = NULL;
  gboolean ret;

  g_assert (SP_IS_SSH_CONNECTION (connection));

  ret = sp_ssh_connection_close_finish (connection, result, &error);

  g_assert_no_error (error);
  g_assert_cmpint (ret, ==, TRUE);

  g_main_loop_quit (main_loop);
}

static void
execute_cb (GObject      *object,
            GAsyncResult *result,
            gpointer      user_data)
{
  SpSshConnection *connection = (SpSshConnection *)object;
  g_autoptr(GError) error = NULL;
  g_autoptr(GOutputStream) stdin_stream = NULL;
  g_autoptr(GInputStream) stdout_stream = NULL;
  g_autoptr(GInputStream) stderr_stream = NULL;
  gboolean ret;

  g_assert (SP_IS_SSH_CONNECTION (connection));

  ret = sp_ssh_connection_execute_finish (connection, result, &stdin_stream, &stdout_stream, &stderr_stream, &error);

  g_assert_no_error (error);
  g_assert_cmpint (ret, ==, TRUE);

  g_printerr ("Command executed\n");

  sp_ssh_connection_close_async (connection, NULL, on_close_cb, NULL);
}

static void
on_connection_closed (SpSshConnection *connection,
                      GParamSpec      *pspec,
                      gpointer         user_data)
{
  g_assert (SP_IS_SSH_CONNECTION (connection));
  g_assert (pspec != NULL);

  if (sp_ssh_connection_get_closed (connection))
    g_print ("Connection closed\n");
}

static void
connect_cb (GObject      *object,
            GAsyncResult *result,
            gpointer      user_data)
{
  SpSshClient *client = (SpSshClient *)object;
  g_autoptr(SpSshConnection) connection = NULL;
  g_autoptr(GError) error = NULL;
  const gchar *command = "touch /tmp/foo";
  const gchar * env[] = { NULL };

  g_assert (SP_IS_SSH_CLIENT (client));

  connection = sp_ssh_client_connect_finish (client, result, &error);

  g_assert_no_error (error);
  g_assert (SP_IS_SSH_CONNECTION (connection));

  g_printerr ("Connected, executing command\n");

  g_signal_connect (connection,
                    "notify::closed",
                    G_CALLBACK (on_connection_closed),
                    NULL);

  sp_ssh_connection_execute_async (connection, command, env, NULL, execute_cb, NULL);
}

gint
main (gint argc,
      gchar *argv[])
{
  g_autoptr(SpSshClient) client = NULL;
  g_autoptr(GSocketConnectable) connectable = NULL;

  if (0 != libssh2_init (0))
    g_error ("Failed to initialize libssh2");

  main_loop = g_main_loop_new (NULL, FALSE);

  client = sp_ssh_client_new ();

  connectable = g_network_address_new ("localhost", 22);

  sp_ssh_client_connect_async (client, connectable, NULL, connect_cb, NULL);

  g_main_loop_run (main_loop);
  g_main_loop_unref (main_loop);

  return 0;
}
