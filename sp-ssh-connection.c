/* sp-ssh-connection.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libssh2.h>

#include "sp-ssh-connection.h"
#include "sp-ssh-connection-private.h"
#include "sp-ssh-error.h"

#define SHUTDOWN_MESSAGE GINT_TO_POINTER(1234)

struct _SpSshConnection
{
  GObject            parent_instance;

  /* Accessable from main and worker thread */
  GAsyncQueue       *queue;

  /* Access from main thread only */
  GSocketConnection *connection;
  GThread           *worker;
};

typedef struct
{
  GSocketConnection *connection;
  LIBSSH2_SESSION *session;
  int fd;
} SpSshConnectionState;

typedef enum
{
  SP_SSH_COMMAND_HANDSHAKE = 1,
  SP_SSH_COMMAND_AUTHENTICATE,
  SP_SSH_COMMAND_CLOSE,
  SP_SSH_COMMAND_EXECUTE,
} SpSshCommandType;

typedef struct
{
  SpSshCommandType type;
  GTask *task;
  union {
    struct {
      SpSshCredentials *credentials;
    } authenticate;
    struct {
      gchar *command;
      gchar **env;
    } execute;
  } u;
} SpSshCommand;

static void          sp_ssh_connection_finalize      (GObject              *object);
static void          sp_ssh_connection_submit        (SpSshConnection      *self,
                                                      SpSshCommand         *command);
static SpSshCommand *sp_ssh_command_new_close        (GTask                *task);
static SpSshCommand *sp_ssh_command_new_handshake    (GTask                *task);
static SpSshCommand *sp_ssh_command_new_authenticate (GTask                *task,
                                                      SpSshCredentials     *credentials);
static SpSshCommand *sp_ssh_command_new_execute      (GTask                *task,
                                                      const gchar          *command,
                                                      const gchar * const  *env);
static void          sp_ssh_command_free             (SpSshCommand         *command);
static void          sp_ssh_command_run              (SpSshCommand         *command,
                                                      SpSshConnectionState *state);
static void          on_connection_notify_closed     (SpSshConnection      *self,
                                                      GParamSpec           *pspec,
                                                      gpointer              source_object);

enum {
  PROP_0,
  PROP_CLOSED,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

G_DEFINE_AUTOPTR_CLEANUP_FUNC (SpSshCommand, sp_ssh_command_free)
G_DEFINE_TYPE (SpSshConnection, sp_ssh_connection, G_TYPE_OBJECT)

static void
sp_ssh_command_free (SpSshCommand *command)
{
  if (command == NULL)
    return;

  switch (command->type)
    {
    case SP_SSH_COMMAND_AUTHENTICATE:
      g_clear_object (&command->u.authenticate.credentials);
      break;

    case SP_SSH_COMMAND_EXECUTE:
      g_clear_pointer (&command->u.execute.command, g_free);
      g_clear_pointer (&command->u.execute.env, g_strfreev);
      break;

    case SP_SSH_COMMAND_HANDSHAKE:
    case SP_SSH_COMMAND_CLOSE:
      break;

    default:
      g_assert_not_reached ();
    }

  g_clear_object (&command->task);
  g_slice_free (SpSshCommand, command);
}

static SpSshCommand *
sp_ssh_command_new_handshake (GTask *task)
{
  SpSshCommand *command;

  g_assert (G_IS_TASK (task));

  command = g_slice_new0 (SpSshCommand);
  command->task = g_object_ref (task);
  command->type = SP_SSH_COMMAND_HANDSHAKE;

  return command;
}

static SpSshCommand *
sp_ssh_command_new_close (GTask *task)
{
  SpSshCommand *command;

  g_assert (G_IS_TASK (task));

  command = g_slice_new0 (SpSshCommand);
  command->task = g_object_ref (task);
  command->type = SP_SSH_COMMAND_CLOSE;

  return command;
}

static SpSshCommand *
sp_ssh_command_new_execute (GTask               *task,
                            const gchar         *command_to_exec,
                            const gchar * const *env)
{
  SpSshCommand *command;

  g_assert (G_IS_TASK (task));

  command = g_slice_new0 (SpSshCommand);
  command->task = g_object_ref (task);
  command->type = SP_SSH_COMMAND_EXECUTE;
  command->u.execute.command = g_strdup (command_to_exec);
  command->u.execute.env = g_strdupv ((gchar **)env);

  return command;
}

static SpSshCommand *
sp_ssh_command_new_authenticate (GTask            *task,
                                 SpSshCredentials *credentials)
{
  SpSshCommand *command;

  g_assert (G_IS_TASK (task));

  command = g_slice_new0 (SpSshCommand);
  command->task = g_object_ref (task);
  command->type = SP_SSH_COMMAND_AUTHENTICATE;
  command->u.authenticate.credentials = g_object_ref (credentials);

  return command;
}

static void
sp_ssh_connection_finalize (GObject *object)
{
  SpSshConnection *self = (SpSshConnection *)object;

  g_clear_object (&self->connection);

  g_async_queue_push (self->queue, SHUTDOWN_MESSAGE);
  g_clear_pointer (&self->queue, g_async_queue_unref);

  g_clear_pointer (&self->worker, g_thread_unref);

  G_OBJECT_CLASS (sp_ssh_connection_parent_class)->finalize (object);
}

static void
sp_ssh_connection_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  SpSshConnection *self = SP_SSH_CONNECTION (object);

  switch (prop_id)
    {
    case PROP_CLOSED:
      g_value_set_boolean (value, sp_ssh_connection_get_closed (self));

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sp_ssh_connection_class_init (SpSshConnectionClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = sp_ssh_connection_finalize;
  object_class->get_property = sp_ssh_connection_get_property;

  properties [PROP_CLOSED] =
    g_param_spec_boolean ("closed",
                          "Closed",
                          "Closed",
                          FALSE,
                          (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
sp_ssh_connection_init (SpSshConnection *self)
{
  self->queue = g_async_queue_new ();
}

static void
sp_ssh_command_run_handshake (SpSshCommand         *command,
                              SpSshConnectionState *state)
{
  const gchar *fingerprint;

  g_assert (command != NULL);
  g_assert (state != NULL);

  if (state->session != NULL)
    {
      g_task_return_new_error (command->task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVAL,
                               "Cannot handshake, session active");
      return;
    }

  state->session = libssh2_session_init ();

  if (state->session == NULL)
    {
      g_task_return_new_error (command->task,
                               G_IO_ERROR,
                               G_IO_ERROR_FAILED,
                               "Failed to create libssh2 session");
      return;
    }

  libssh2_session_set_blocking (state->session, 1);

  for (;;)
    {
      int rc;

      rc = libssh2_session_handshake (state->session, state->fd);
      if (rc == LIBSSH2_ERROR_EAGAIN)
        continue;

      if (rc != 0)
        {
          g_task_return_new_error (command->task,
                                   G_IO_ERROR,
                                   G_IO_ERROR_FAILED,
                                   "Failed to handshake session");
          return;
        }

      break;
    }

  fingerprint = libssh2_hostkey_hash (state->session, LIBSSH2_HOSTKEY_HASH_SHA1);

  for (guint i = 0; i < 20; i++)
    g_print ("%02X ", (guint)((const guint8 *)fingerprint)[i]);
  g_print("\n");

  g_task_return_boolean (command->task, TRUE);
}

static gboolean
sp_ssh_command_check_session (SpSshCommand         *command,
                              SpSshConnectionState *state)
{
  g_assert (command != NULL);
  g_assert (state != NULL);

  if (state->session == NULL)
    {
      g_task_return_new_error (command->task,
                               G_IO_ERROR,
                               G_IO_ERROR_INVAL,
                               "No active session, cannot perform operation");
      return FALSE;
    }

  return TRUE;
}

static void
sp_ssh_command_run_authenticate (SpSshCommand         *command,
                                 SpSshConnectionState *state)
{
  g_autoptr(GError) error = NULL;

  g_assert (command != NULL);
  g_assert (command->type == SP_SSH_COMMAND_AUTHENTICATE);
  g_assert (SP_IS_SSH_CREDENTIALS (command->u.authenticate.credentials));
  g_assert (state != NULL);

  if (!sp_ssh_command_check_session (command, state))
    return;

  if (!sp_ssh_credentials_authenticate (command->u.authenticate.credentials,
                                        state->session, &error))
    {
      g_task_return_error (command->task, g_steal_pointer (&error));
      return;
    }

  g_task_return_boolean (command->task, TRUE);
}

static void
sp_ssh_command_run_close (SpSshCommand         *command,
                          SpSshConnectionState *state)
{
  g_assert (command != NULL);
  g_assert (command->type == SP_SSH_COMMAND_CLOSE);
  g_assert (state != NULL);

  if (state->session != NULL)
    {
      libssh2_session_disconnect (state->session, "closing libssh2-glib session");
      libssh2_session_free (state->session);
      state->session = NULL;
    }

  if (state->connection != NULL)
    {
      if (!g_io_stream_is_closed (G_IO_STREAM (state->connection)))
        {
          GCancellable *cancellable;
          GError *error = NULL;

          cancellable = g_task_get_cancellable (command->task);

          if (!g_io_stream_close (G_IO_STREAM (state->connection), cancellable, &error))
            {
              g_task_return_error (command->task, error);
              return;
            }
        }
    }

  g_task_return_boolean (command->task, TRUE);
}

static void
sp_ssh_command_run_execute (SpSshCommand         *command,
                            SpSshConnectionState *state)
{
  g_autoptr(GError) error = NULL;
  LIBSSH2_CHANNEL *channel;
  guint i;
  gint rc;

  g_assert (command != NULL);
  g_assert (command->type == SP_SSH_COMMAND_EXECUTE);
  g_assert (command->u.execute.command != NULL);
  g_assert (state != NULL);

  if (!sp_ssh_command_check_session (command, state))
    return;

  channel = libssh2_channel_open_session (state->session);

  if (channel == NULL)
    {
      gint last_errno = libssh2_session_last_errno (state->session);

      g_task_return_new_error (command->task,
                               SP_SSH_ERROR,
                               last_errno,
                               "%s", g_strerror (last_errno));
      return;
    }

  if (command->u.execute.env != NULL)
    {
      for (i = 0; command->u.execute.env[i]; i++)
        {
          const gchar *envstr = command->u.execute.env[i];
          const gchar *eq = strchr (envstr, '=');

          if (eq == NULL)
            continue;

          libssh2_channel_setenv_ex (channel,
                                     envstr,
                                     eq - envstr,
                                     eq + 1,
                                     strlen (eq + 1));
        }
    }

  rc = libssh2_channel_exec (channel, command->u.execute.command);

  if (rc != 0)
    {
      g_task_return_new_error (command->task,
                               SP_SSH_ERROR,
                               rc,
                               "Execution of command failed: %s\n",
                               command->u.execute.command);
      goto cleanup;
    }

  g_task_return_boolean (command->task, TRUE);

cleanup:
  libssh2_channel_close (channel);
  libssh2_channel_wait_closed (channel);
  libssh2_channel_free (channel);
}

static void
sp_ssh_command_run (SpSshCommand         *command,
                    SpSshConnectionState *state)
{
  g_assert (command != NULL);
  g_assert (state != NULL);

  switch (command->type)
    {
    case SP_SSH_COMMAND_HANDSHAKE:
      sp_ssh_command_run_handshake (command, state);
      break;

    case SP_SSH_COMMAND_AUTHENTICATE:
      sp_ssh_command_run_authenticate (command, state);
      break;

    case SP_SSH_COMMAND_CLOSE:
      sp_ssh_command_run_close (command, state);
      break;

    case SP_SSH_COMMAND_EXECUTE:
      sp_ssh_command_run_execute (command, state);
      break;

    default:
      g_assert_not_reached ();
      break;
    }
}

static gpointer
sp_ssh_connection_worker (gpointer data)
{
  g_autoptr(SpSshConnection) self = data;
  g_autoptr(GAsyncQueue) queue = NULL;
  g_autoptr(GSocketConnection) connection = NULL;
  SpSshConnectionState state = { 0 };
  GSocket *sock;
  gpointer commandptr;

  g_assert (SP_IS_SSH_CONNECTION (self));

  /*
   * We own a reference to @self at this point, so we will never be finalized
   * until we release our reference. Once we get references on the structures
   * we care about, we can release our reference. Once %NULL is received on
   * the async queue, we know we need to exit and allow our thread to be
   * joined by the main thread.
   */
  queue = g_async_queue_ref (self->queue);
  connection = g_object_ref (self->connection);
  state.connection = connection;

  /*
   * We need raw socket access for use with libssh2 APIs.
   */
  sock = g_socket_connection_get_socket (connection);
  state.fd = g_socket_get_fd (sock);

  /*
   *  Now release our reference allowing @self to be finalized when our peer
   *  thread is no longer using it.
   */
  g_clear_object (&self);

  while (SHUTDOWN_MESSAGE != (commandptr = g_async_queue_pop (queue)))
    {
      g_autoptr(SpSshCommand) command = commandptr;
      sp_ssh_command_run (command, &state);
    }

  return NULL;
}

static void
on_connection_notify_closed (SpSshConnection   *self,
                             GParamSpec        *pspec,
                             gpointer           source_object)
{
  g_assert (SP_IS_SSH_CONNECTION (self));
  g_assert (G_IS_SOCKET_CONNECTION (source_object) || G_IS_TASK (source_object));

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CLOSED]);
}

SpSshConnection *
_sp_ssh_connection_new (GSocketConnection *connection)
{
  SpSshConnection *self;

  g_return_val_if_fail (G_IS_SOCKET_CONNECTION (connection), NULL);

  self = g_object_new (SP_TYPE_SSH_CONNECTION, NULL);
  self->connection = g_object_ref (connection);
  self->worker = g_thread_new ("[SpSshConnection] worker",
                               sp_ssh_connection_worker,
                               g_object_ref (self));

  g_signal_connect_object (self->connection,
                           "notify::closed",
                           G_CALLBACK (on_connection_notify_closed),
                           self,
                           G_CONNECT_SWAPPED);

  return self;
}

/*
 * sp_ssh_connection_submit:
 * @self: A #SpSshConnection
 * @command: (transfer full): An #SpSshCommand.
 *
 * Submits @command into the workqueue. This steals the reference to
 * @command and it should no longer be used after calling this function.
 */
static void
sp_ssh_connection_submit (SpSshConnection *self,
                          SpSshCommand    *command)
{
  g_assert (SP_IS_SSH_CONNECTION (self));
  g_assert (command != NULL);

  g_async_queue_push (self->queue, command);
}

void
sp_ssh_connection_execute_async (SpSshConnection     *self,
                                 const gchar         *command_to_exec,
                                 const gchar * const *env,
                                 GCancellable        *cancellable,
                                 GAsyncReadyCallback  callback,
                                 gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (SP_IS_SSH_CONNECTION (self));
  g_return_if_fail (command_to_exec != NULL);
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, sp_ssh_connection_execute_async);

  sp_ssh_connection_submit (self, sp_ssh_command_new_execute (task, command_to_exec, env));
}

gboolean
sp_ssh_connection_execute_finish (SpSshConnection  *self,
                                  GAsyncResult     *result,
                                  GOutputStream   **stdin_stream,
                                  GInputStream    **stdout_stream,
                                  GInputStream    **stderr_stream,
                                  GError          **error)
{
  g_return_val_if_fail (SP_IS_SSH_CONNECTION (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  if (stdin_stream != NULL)
    *stdin_stream = NULL;

  if (stdout_stream != NULL)
    *stdout_stream = NULL;

  if (stderr_stream != NULL)
    *stderr_stream = NULL;

  return g_task_propagate_boolean (G_TASK (result), error);
}

void
_sp_ssh_connection_handshake_async (SpSshConnection     *self,
                                    GCancellable        *cancellable,
                                    GAsyncReadyCallback  callback,
                                    gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (SP_IS_SSH_CONNECTION (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, _sp_ssh_connection_handshake_async);

  sp_ssh_connection_submit (self, sp_ssh_command_new_handshake (task));
}

gboolean
_sp_ssh_connection_handshake_finish (SpSshConnection  *self,
                                     GAsyncResult     *result,
                                     GError          **error)
{
  g_return_val_if_fail (SP_IS_SSH_CONNECTION (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

void
_sp_ssh_connection_authenticate_async (SpSshConnection     *self,
                                       SpSshCredentials    *credentials,
                                       GCancellable        *cancellable,
                                       GAsyncReadyCallback  callback,
                                       gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (SP_IS_SSH_CONNECTION (self));
  g_return_if_fail (SP_IS_SSH_CREDENTIALS (credentials));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, _sp_ssh_connection_authenticate_async);

  sp_ssh_connection_submit (self, sp_ssh_command_new_authenticate (task, credentials));
}

gboolean
_sp_ssh_connection_authenticate_finish (SpSshConnection  *self,
                                        GAsyncResult     *result,
                                        GError          **error)
{
  g_return_val_if_fail (SP_IS_SSH_CONNECTION (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

gboolean
sp_ssh_connection_get_closed (SpSshConnection *self)
{
  g_return_val_if_fail (SP_IS_SSH_CONNECTION (self), TRUE);

  if (self->connection != NULL)
    return g_io_stream_is_closed (G_IO_STREAM (self->connection));

  return TRUE;
}

void
sp_ssh_connection_close_async (SpSshConnection     *self,
                               GCancellable        *cancellable,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  g_return_if_fail (SP_IS_SSH_CONNECTION (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, sp_ssh_connection_close_async);

  g_signal_connect_object (task,
                           "notify::completed",
                           G_CALLBACK (on_connection_notify_closed),
                           self,
                           G_CONNECT_SWAPPED);

  sp_ssh_connection_submit (self, sp_ssh_command_new_close (task));
}

gboolean
sp_ssh_connection_close_finish (SpSshConnection  *self,
                                GAsyncResult     *result,
                                GError          **error)
{
  g_return_val_if_fail (SP_IS_SSH_CONNECTION (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (result), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}
