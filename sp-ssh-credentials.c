/* sp-ssh-credentials.c
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sp-ssh-credentials.h"

G_DEFINE_INTERFACE (SpSshCredentials, sp_ssh_credentials, G_TYPE_OBJECT)

static void
sp_ssh_credentials_default_init (SpSshCredentialsInterface *iface)
{
}

gboolean
sp_ssh_credentials_authenticate (SpSshCredentials  *self,
                                 gpointer           native,
                                 GError           **error)
{
  g_return_val_if_fail (SP_IS_SSH_CREDENTIALS (self), FALSE);
  g_return_val_if_fail (native != NULL, FALSE);

  return SP_SSH_CREDENTIALS_GET_IFACE (self)->authenticate (self, native, error);
}
