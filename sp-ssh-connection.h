/* sp-ssh-connection.h
 *
 * Copyright (C) 2016 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SP_SSH_CONNECTION_H
#define SP_SSH_CONNECTION_H

#include <gio/gio.h>

#include "sp-ssh-types.h"

G_BEGIN_DECLS

#define SP_TYPE_SSH_CONNECTION (sp_ssh_connection_get_type())

G_DECLARE_FINAL_TYPE (SpSshConnection, sp_ssh_connection, SP, SSH_CONNECTION, GObject)

void          sp_ssh_connection_execute_async       (SpSshConnection      *self,
                                                     const gchar          *command_to_exec,
                                                     const gchar * const  *env,
                                                     GCancellable         *cancellable,
                                                     GAsyncReadyCallback   callback,
                                                     gpointer              user_data);
gboolean      sp_ssh_connection_execute_finish      (SpSshConnection      *self,
                                                     GAsyncResult         *result,
                                                     GOutputStream       **stdin_stream,
                                                     GInputStream        **stdout_stream,
                                                     GInputStream        **stderr_stream,
                                                     GError              **error);
void          sp_ssh_connection_close_async         (SpSshConnection      *self,
                                                     GCancellable         *cancellable,
                                                     GAsyncReadyCallback   callback,
                                                     gpointer              user_data);
gboolean      sp_ssh_connection_close_finish        (SpSshConnection      *self,
                                                     GAsyncResult         *result,
                                                     GError              **error);
gboolean      sp_ssh_connection_get_closed          (SpSshConnection      *self);

G_END_DECLS

#endif /* SP_SSH_CONNECTION_H */
